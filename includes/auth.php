<?php

require_once('../includes/config.php');

$user = $_SERVER['PHP_AUTH_USER'];
$pass = $_SERVER['PHP_AUTH_PW'];

$validated = ($user == $auth_user) && ($pass == $auth_pass);

if (!$validated) {
  header('WWW-Authenticate: Basic realm="Wrong pass, no shell"');
  header('HTTP/1.0 401 Unauthorized');
  die ("Not authorized");
}

unset($user,$pass,$validated);

?>
