<?php

require_once('../includes/config.php');

class MySQLTerm {
	public $term;
	
	function __construct($str) {
		$this->term=$str;
	}
}

class Database {
	private $mysqli;
	
	function __construct() {
		global $db_user, $db_pass, $db_name;
		
		$this->mysqli=new mysqli("localhost", $db_user, $db_pass, $db_name);
		
		if(mysqli_connect_errno()) {
			die('[400,"sql connection failed","'.mysqli_connect_error().'"]');
		}

		if(!$this->mysqli->set_charset("utf8")){
			die('[401,"utf8 load failed ","'.$this->mysqli->error.'"]');
		}
	}
	
	function __destruct() {
		$this->mysqli->close();
	}
	
	private function query($string){
		$res=$this->mysqli->query($string);
		if($res === false){
			die('[402,"wrong query","'.$string.'"]');
		}
		return $res;
	}
	
	public function select($fields,$matches,$table="item",$order="",$limit=""){
		if(gettype($fields)=="array"){
			foreach ($fields as $key => $value){
				$fields[$key] = $this->mysqli->real_escape_string($value);
			}
			$fields='`'.implode("`, `",$fields).'`';
		}else{
			$fields = $this->mysqli->real_escape_string($fields);
		}
		
		if(gettype($matches)=="array"){
			$str='';
			foreach ($matches as $key => $value){
				if($str){
					$str=$str.' AND ';
				}
				$str=$str.$key;
				if(gettype($value)=="array"){
					foreach ($value as $k => $v){
						if(gettype($v)=="array"){
							$value[$k] = $this->mysqli->real_escape_string(implode('',$v));
						}else{
							$value[$k] = $this->mysqli->real_escape_string($v);
						}
					}
					$str=$str." IN ('".implode("', '",$value)."')";
				}else{
					if(gettype($value)=="object"){
						$str=$str.'='.$value->term;
					}else{
						$str=$str.'=\''.$this->mysqli->real_escape_string($value).'\'';
					}
				}
			}
			$matches=$str;
		}else{
			$matches = $this->mysqli->real_escape_string($matches);
		}
		
		$limit_q='';
		if($limit){
			$limit = $this->mysqli->real_escape_string($limit);
			$limit_q=' LIMIT '.$limit;
		}
		
		$table = $this->mysqli->real_escape_string($table);
		
		$order_q='';
		if($order){
			if(gettype($order)=="object"){
				$order_q=' ORDER BY '.$order->term;
			}else if(gettype($order)=="array"){
				foreach($order as $k=>$v){
					$order[$k]=$this->mysqli->real_escape_string($v);
				}
				$order_q=' ORDER BY `'.implode('`,`',$order).'`';
			}else{
				if(substr($order,0,1)=='!'){
					$order_q=' DESC';
					$order=substr($order,1);
				}
				$order = $this->mysqli->real_escape_string($order);
				$order_q=' ORDER BY `'.$order.'`'.$order_q;
			}
		}
		
		$res = $this->query('SELECT '.$fields.' FROM `'.$table.'` WHERE '.$matches.$order_q.$limit_q);
		$res_arr=array();
		while ($data = $res->fetch_assoc()){
			$res_arr[] = $data;
		}
		return $res_arr;
	}
	
	public function update($fields,$matches,$table){
		if(gettype($fields)=="array"){
			$str='';
			foreach ($fields as $key => $value){
				if($str){
					$str=$str.',';
				}
				$str=$str.'`'.$key.'`=\''.$this->mysqli->real_escape_string($value).'\'';
			}
			$fields=$str;
		}else{
			$fields = $this->mysqli->real_escape_string($fields);
		}
		
		if(gettype($matches)=="array"){
			$str='';
			foreach ($matches as $key => $value){
				if($str){
					$str=$str.' AND ';
				}
				$str=$str.'`'.$this->mysqli->real_escape_string($key).'`';
				if(gettype($value)=="array"){
					foreach ($value as $k => $v){
						if(gettype($v)=="array"){
							$value[$k] = $this->mysqli->real_escape_string(implode('',$v));
						}else{
							$value[$k] = $this->mysqli->real_escape_string($v);
						}
					}
					$str=$str." IN ('".implode("', '",$value)."')";
				}else{
					$str=$str.'=\''.$this->mysqli->real_escape_string($value).'\'';
				}
			}
			$matches=$str;
		}else{
			$matches = $this->mysqli->real_escape_string($matches);
		}
		
		$table = $this->mysqli->real_escape_string($table);
		
		$res = $this->query('UPDATE `'.$table.'` SET '.$fields.' WHERE '.$matches);
		
		return $res;
	}
	
	public function insert($fields,$table){
		if(gettype($fields)=="array"){
			$keys=array_keys($fields);
			$values=array_values($fields);
			foreach($keys as $k => $v){
				$keys[$k]=$this->mysqli->real_escape_string($v);
			}
			foreach($values as $k => $v){
				$values[$k]=$this->mysqli->real_escape_string($v);
			}
			$fields='`'.implode('`, `',$keys).'`';
			$values='\''.implode('\', \'',$values).'\'';
		}else{
			die('[403,"wrong value type"]');
		}
		
		$table = $this->mysqli->real_escape_string($table);
		
		$res = $this->query('INSERT INTO `'.$table.'`('.$fields.') VALUES ('.$values.')');
		
		if($res){
			return $this->mysqli->insert_id;
		}else{
			return false;
		}
	}
	
	public function count($matches,$table){
		$res=$this->select('COUNT(*)',$matches,$table);
		return $res[0]['COUNT(*)'];
	}
	
	public function find($fields,$matches,$s_fields,$query,$table="item",$order="",$limit=""){
		$query="'".$this->mysqli->real_escape_string($query)."'";
		foreach($s_fields as $k => $v){
			$s_fields[$k]=$this->mysqli->real_escape_string($v);
		}
		if(gettype($matches)=="array"){
			$matches['MATCH ('.implode(',',$s_fields).') AGAINST ('.$query.')!']=0;
		}else{
			$matches='MATCH ('.implode(',',$s_fields).') AGAINST ('.$query.') AND '.$matches;
		}
		return $this->select($fields,$matches,$table,$order,$limit);
	}
	
	public function count_find($matches,$s_fields,$query,$table="item"){
		$query="'".$this->mysqli->real_escape_string($query)."'";
		foreach($s_fields as $k => $v){
			$s_fields[$k]=$this->mysqli->real_escape_string($v);
		}
		if(gettype($matches)=="array"){
			$matches['MATCH ('.implode(',',$s_fields).') AGAINST ('.$query.')!']=0;
		}else{
			$matches='MATCH ('.implode(',',$s_fields).') AGAINST ('.$query.') AND '.$matches;
		}
		$res=$this->select('COUNT(*)',$matches,$table);
		return $res[0]['COUNT(*)'];
	}
	
	public function fields($table="item"){
		$table = $this->mysqli->real_escape_string($table);
		
		$res = $this->query('SHOW COLUMNS FROM `'.$table.'`');
		$res_arr=array();
		while ($data = $res->fetch_row()){
			$res_arr[] = $data[0];
		}
		
		return $res_arr;
	}
}

$db=new Database();

?>
