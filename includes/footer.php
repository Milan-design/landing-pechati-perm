<div class="lightbox_container">
	<div class="valignHelper"></div>
	<div class="lightbox_order">
		<div class="lightbox_title">
			<h4>Оставить заявку</h4>
			<a href="#" class="lightbox_close">✕</a>
		</div>
		<form class="lightbox_form callback_form">
			<input type="text" name="fio" id="fio" placeholder="Ваше имя"><label for="fio"></label>
            <input type="tel" name="phone" id="phone" placeholder="Ваш телефон"><label for="phone"></label>
            <input type="hidden" name="item">
            <input type="hidden" name="promo_id">
            <input type="hidden" name="promo_code">
            <input type="submit" value="Заказать печать">
		</form>
		<div class="lightbox_promo_result_container">
			<div class="valignHelper"></div>
			<div class="lightbox_promo_result">
				<p class="simple">Ваша скидка:</p>
				<h1 class="yellow">0%</h1>
				<a href="#block3" class="lightbox_close goto gotoPromo">Получить скидку!</a>
			</div>
		</div>
	</div>
	<div class="lightbox_callback">
		<div class="lightbox_title">
			<h4>Перезвоните мне</h4>
			<a href="#" class="lightbox_close">✕</a>
		</div>
		<form class="lightbox_form callback_form">
			<input type="text" name="fio" id="fio" placeholder="Ваше имя"><label for="fio"></label>
            <input type="tel" name="phone" id="phone" placeholder="Ваш телефон"><label for="phone"></label>
            <input type="submit" value="Перезвоните мне">
		</form>
	</div>
</div>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg">
	<filter id="blur">
	<feGaussianBlur stdDeviation="6" />
	</filter>
</svg>
<div class="backButton">
	<a href="#block0">
		<div></div>
		<br>
		Наверх
	</a>
</div>