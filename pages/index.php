<html>
<head>
	<? require "../includes/headers.php"; ?>
</head>
<body>
	<div class="siteBlock_wrapper">

<?

$blocks=array(
	array('bg'=>'Menu','overlap'=>1),
	array('bg'=>'People','helper'=>1),
	array('bg'=>'Paper'),
	array('bg'=>'Gray1'),
	array('bg'=>'Lightning','helper'=>1),
	array('bg'=>'Paper'),
	array('bg'=>'Gray2','helper'=>1),
	array('bg'=>'Stars'),
	array('bg'=>'Paper','helper'=>1),
	array('bg'=>'People_dark'),
	array('fullsize'=>1),
	array('bg'=>'Yellow','overlap'=>1)
);

$block_id=0;
foreach ($blocks as $key => $block) {
	if(!isset($block['disabled'])){
		if(isset($block['helper'])){
			require_once('../helpers/block'.$key.'.php');
		}
		if(isset($block['overlap'])){
			$overlap=' block'.$key.'_overlap';
		}else{
			$overlap='';
		}
		if(isset($block['bg'])){
			$bg=' bg'.$block['bg'];
		}else{
			$bg='';
		}
		if(isset($block['fullsize'])){
			$content='content_fullsize';
		}else{
			$content='content';
		}
		?>
			<div class="siteBlock_container block<?= $key ?>_h<?= $overlap ?>" id="block<?= $block_id ?>">
		<? if(!isset($block['empty'])){ ?>
				<div class="siteBlock<?= $bg ?>">
					<div class="<?= $content ?>">
		<? 
				require_once('../tmpl/block'.$key.'.php');
		?>			
					</div>
				</div>
		<? } ?>
			</div>
		<?
		++$block_id;
	}
}

?>

</div>
<? require "../includes/footer.php"; ?>
</body>
</html>