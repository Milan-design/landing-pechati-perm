<?

require('../includes/auth.php');

?>
<html>
<head>
	<? require('../includes/headers_admin.php'); ?>
</head>
<body>
<?

require('../includes/database.php');

$tables=array('advice'=>'Советы при покупке','comment'=>'Отзывы','item'=>'Товары','promo_results'=>'Результаты викторины');

$textarea=array('descr','shortdescr','text');

foreach ($tables as $table => $tableName) {

	?>
	<h1><?= $tableName ?></h1>
	<ol>
	<?

	$values=$db->select('*',1,$table);
	foreach ($values as $value) {
		?>
		<li>
		<form>
		<?
		foreach ($value as $field => $fieldValue) {
			if(in_array($field, $textarea)){
				?>
				<textarea name="<?= $field ?>"><?= $fieldValue ?></textarea><br>
				<?
			}else{
				?>
				<input type="text" name="<?= $field ?>" value="<?= $fieldValue ?>"><br>
				<?
			}
		}
		?>
		<input type="submit" value="Сохранить"><br>
		</form>
		</li>
		<?
	}
	?>
	</ol>
	<form>
	<?
	$fields=$db->fields($table);
	foreach ($fields as $field) {
		if(in_array($field, $textarea)){
			?>
			<textarea name="<?= $field ?>"></textarea><br>
			<?
		}else{
			?>
			<input type="text" name="<?= $field ?>"><br>
			<?
		}
	}
	?>
	<input type="submit" value="Добавить"><br>
	</form>
	<?
}

?>
</body>
</html>