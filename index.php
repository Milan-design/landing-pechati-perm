<html>
<head>
	<? require_once "headers.php"; ?>
</head>
<body>
	<div class="siteBlock_wrapper">

<?

$blocks=array(
	array('bg'=>'Menu','overlap'=>1),
	array('bg'=>'People'),
	array('bg'=>'Paper'),
	array('bg'=>'Gray1'),
	array('bg'=>'Lightning'),
	array('bg'=>'Paper'),
	array('bg'=>'Gray2'),
	array('bg'=>'Stars'),
	array('bg'=>'Paper'),
	array('empty'=>1)
);

foreach ($blocks as $key => $block) {
	if(isset($block['helper'])){
		require_once('helpers/block'.$key.'.php');
	}
	if(isset($block['overlap'])){
		$overlap=' block'.$key.'_overlap';
	}else{
		$overlap='';
	}
	if(isset($block['bg'])){
		$bg=' bg'.$block['bg'];
	}else{
		$bg='';
	}
	?>
		<div class="siteBlock_container block<?= $key ?>_h<?= $overlap ?>">
	<? if(!isset($block['empty'])){ ?>
			<div class="siteBlock<?= $bg ?>">
				<div class="content">
	<? 
			require_once('tmpl/block'.$key.'.php');
	?>			
				</div>
			</div>
	<? } ?>
		</div>
	<?
}

?>

</div>
</body>
</html>