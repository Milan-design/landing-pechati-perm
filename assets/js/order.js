$(document).ready(function(){
	$('.orderShow').click(function(e){
		var id=$(this).attr('data-id');
		if(!id){
			id=$('#orderButton').attr('data-id');
		}

		var promo_id=$(this).attr('data-promo-id');
		var promo_code=$(this).attr('data-promo-code');
		var promo_percent=$(this).attr('data-promo-percent');
		if(!promo_id){
			promo_id='';
			promo_code='';
			promo_percent='0%';
		}

		var form=$('.lightbox_order .lightbox_form')[0];

		form.item.value=id;
		form.promo_id.value=promo_id;
		form.promo_code.value=promo_code;

		if(promo_id){
			$(form).find('input[type=submit]').val('Заказать печать со скидкой');
			$('a.gotoPromo').hide();
		}else{
			$(form).find('input[type=submit]').val('Заказать печать без скидки');
			$('a.gotoPromo').show();
		}
		$('.lightbox_promo_result .yellow').html(promo_percent);

		$('.lightbox_order').css({display:'inline-block'});
		$('.lightbox_container').fadeIn();
		$('.siteBlock_wrapper').addClass('blur');
		$('.backButton').hide();

		e.preventDefault();
	});

	$('.callbackShow').click(function(e){
		$('.lightbox_callback').css({display:'inline-block'});
		$('.lightbox_container').fadeIn();
		$('.siteBlock_wrapper').addClass('blur');
		$('.backButton').hide();

		e.preventDefault();
	});

	$('.lightbox_close').click(function(e){
		$('.lightbox_container , .lightbox_callback , .lightbox_order').fadeOut().promise().done(function(){
			$(document).trigger('scroll');
		});
		$('.siteBlock_wrapper').removeClass('blur');
		$(document).trigger('scroll');

		e.preventDefault();
	});

	$('.lightbox_container').click(function(e){
		var x=e.pageX-$(document).scrollLeft();
		var y=e.pageY-$(document).scrollTop();
		var closest=$(document.elementFromPoint(x,y));
		if(closest.hasClass('lightbox_container')){
			$('.lightbox_container , .lightbox_callback , .lightbox_order').fadeOut().promise().done(function(){
				$(document).trigger('scroll');
			});
			$('.siteBlock_wrapper').removeClass('blur');
		}
	});
});