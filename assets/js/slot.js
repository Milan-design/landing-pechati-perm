$(document).ready(function(){
	var stop=true;
	var start=false;
	var spin_total=15;
	var spin_after_stop=0;
	var stop_pending=false;

	var result=[];

	var randSeed=217575;
	var randLimits=[34,21,13,8,5,3,2,1];
	var randLimitsSum=87;
	var intLimits=1000007;

	var startTime=0;
	var stopTime=0;

	var delay=100;

	var max_spins=20;

	stopInterval=0;
	stopIntervalMax=1000;
	stopIntervalMin=500;

	var try_count=0;

	function randNum(){
		randSeed=Math.round(Math.sin(1533.1*randSeed+Math.sqrt(1.1*stopTime))*(418713678.1+1.1*spin_after_stop)+Math.sqrt(100500.1+1.1*randSeed*randSeed*12345.1+1.1*spin_total)/2+Math.cos(1.1*randSeed*randSeed)*(Math.sqrt(1.1*startTime)+12345.1))%intLimits;
	}

	function randId(){
		randNum();
		var limit=Math.abs(randSeed)%randLimitsSum+1;
		var i=0;
		while(limit>0){
			limit-=randLimits[i];
			++i;
		}
		return i;
	}

	function randInterval(){
		randNum();
		return Math.abs(randSeed)%(stopIntervalMax-stopIntervalMin)+stopIntervalMin;
	}

	function spinWheel(){
		var div=$(),first=$(),last=$();

		spin_total++;
	
		if((spin_total==max_spins || stop_pending) && !stop){
			stop=true;
			stopTime=startTime+spin_total*delay;
			var spinning_first=$('.iPad_slot_container.spinning').first();
			spinning_first.removeClass('spinning');
			spinning_first.addClass('stopping');
			stopInterval=randInterval();
			stop_pending=false;
		}

		if(stop){
			spin_after_stop++;
			stopInterval-=delay;
			if(stopInterval<=0){
				var stopping=$('.iPad_slot_container.stopping');
				var spinning_first=$('.iPad_slot_container.spinning').first();
				stopping.removeClass('stopping');
				if(spinning_first.length){
					spinning_first.removeClass('spinning');
					spinning_first.addClass('stopping');
					stopInterval=randInterval();
				}else{
					var data={start:startTime,stop:stopTime,result:result};
					$.post('/promo-result',{json:JSON.stringify(data)},function(data){
						var ps=$('.promo_result p');
						if(!data['error']){
							if(data['win']){
								var percent=data['percent']+'%';
								var item=data['item']['name'];
								var itemid=data['item']['id'];
								var id=data['id'];
								var code=data['code'];
								$(ps[0]).html('Ваш выигрыш:');
								$(ps[1]).html('<b>'+percent+'</b>скидка');
								$(ps[2]).html('На <b>'+item+'</b>');
								$('.promo_result_bl , #orderButtonPromo').attr('data-promo-id',id);
								$('.promo_result_bl , #orderButtonPromo').attr('data-promo-code',code);
								$('.promo_result_bl , #orderButtonPromo').attr('data-promo-percent',percent);
								$('.promo_result_bl , #orderButtonPromo').attr('data-id',itemid);
								$('.promo_result_bl , #orderButtonPromo').show();
								$('#orderButtonPromo + label').css({display:'inline-block'});
							}else{
								if(try_count){
									$(ps[0]).html('Осталось');
									$(ps[1]).html('<b>'+try_count+'</b>попыток');
									$(ps[2]).html('Попробуйте снова');
								}else{
									$(ps[0]).html('Использованы');
									$(ps[1]).html('Все попытки');
									$(ps[2]).html('Попробуйте завтра');
									setInterval(function(){
										$('.iPad_content').addClass('iPad_content_disabled')
									},3000);
								}
								$('.promo_result_bl , #orderButtonPromo , #orderButtonPromo + label').hide();
							}
							$('.promo_result').show();
							start=false;
							return;
						}else{
							$(ps[0]).html('Произошла');
							$(ps[1]).html('Ошибка');
							$(ps[2]).html('Перезапустите браузер');
							$('.iPad_content').addClass('iPad_content_disabled');
							$('.promo_result_bl , #orderButtonPromo , #orderButtonPromo + label').hide();
						}
					});
				}
			}
		}

		var spinning=$('.iPad_slot_container.spinning , .iPad_slot_container.stopping');
		if(spinning.length) {
			nextRunSpin=delay;
			div=spinning.children('div');
			first=spinning.children('div:nth-child(2n)');
			last=spinning.children('div:nth-child(2n+1)');
			last.each(function(k,v){
				var id=randId();
				if((!k)&&stop&&(stopInterval<=delay)){
					result.push(id);
				}
				$(v).find('img').attr('src',basepath+id+ext);
			});
			delayC=delay;
		}else{
			return;
		}

		div.animate({top:'0px'},delayC,'linear').promise().done(function(){
			last.css({display:'none'}).promise().done(function(){
				last.each(function(k,v){
					$(v).before($(first[k]));
				}).promise().done(function(){
					div.css({display:'inline-block',top:'-161px'}).promise().done(function(){
						spinWheel();
					});
				});
			});
		});
	}

	var basepath='/assets/img/ico_slot';
	var ext='.png';

	$('.iPad_slot_container img').each(function(k,v){
		var id=Math.round(Math.random()*7)+1;
		$(v).attr('src',basepath+id+ext);
	});

	$('.iPad_stop').click(function(e){
		stop_pending=true;
		e.preventDefault();
	});

	$('.iPad_play').click(function(e){
		if(!start){
			$('.iPad_slot_container').addClass('spinning');
			$.getJSON('/promo-init',function(data){
				if(!data['error']){
					stop=false;
					stop_pending=false;
					start=true;
					spin_total=0;
					spin_after_stop=0;
					startTime=(new Date().getTime())%(1000*60*60);
					stopTime=0;
					result=[];
					randSeed=data['try_seed'];
					try_count=data['try_count'];
					randLimits=data['try_limits'];
					randLimitsSum=data['try_limits_sum'];
					delay=data['try_delay'];
					max_spins=data['try_max_spin'];
					stopIntervalMin=data['try_stop_interval_min'];
					stopIntervalMax=data['try_stop_interval_max'];
					intLimits=data['try_rand_limits'];
					spinWheel();
				}else{
					var ps=$('.promo_result p');
					if(data['error'][0]==1){
						$(ps[0]).html('Использованы');
						$(ps[1]).html('Все попытки');
						$(ps[2]).html('Попробуйте завтра');
					}else{
						$(ps[0]).html('Произошла');
						$(ps[1]).html('Ошибка');
						$(ps[2]).html('Перезапустите браузер');
					}
					$('.promo_result_bl , #orderButtonPromo , #orderButtonPromo + label').hide();
					$('.promo_result').show();
					$('.iPad_content').addClass('iPad_content_disabled');
				}
			});
		}
		e.preventDefault();
	});
	$.getJSON('/promo-check',function(data){
		if(data['error']){
			var ps=$('.promo_result p');
			if(data['error'][0]==1){
				$(ps[0]).html('Использованы');
				$(ps[1]).html('Все попытки');
				$(ps[2]).html('Попробуйте завтра');
			}else{
				$(ps[0]).html('Произошла');
				$(ps[1]).html('Ошибка');
				$(ps[2]).html('Перезапустите браузер');
			}
			$('.promo_result_bl , #orderButtonPromo , #orderButtonPromo + label').hide();
			$('.promo_result').show();
			$('.iPad_content').addClass('iPad_content_disabled');
		}
	});
});