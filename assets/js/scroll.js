$(document).ready(function(){
	var closest=$(document.elementFromPoint(1000,$(window).height()/2)).closest('.siteBlock_container');
	var id=closest.attr('id').replace('block','');
	var max=$('.siteBlock_container').length;

	var mw_event_name=(/Firefox/i.test(navigator.userAgent))?"DOMMouseScroll":"mousewheel";

	$(window).on(mw_event_name,noscroll);

	var delay=1000;

	function backButton(){
		var div=$('.backButton');
		if(id<2){
			div.fadeOut();
		}else{
			div.fadeIn();
		}
		var offset=$(document).scrollTop()+$(window).height();
		var footer=$('.siteBlock_container').last();
		var footerOffset=footer.offset().top-div.height();
		if(offset>footerOffset){
			div.css({position:'absolute',top:footerOffset});
		}else{
			div.css({position:'fixed',bottom:'0px',top:''});
		}
	}

	function scroll(e){
		$(window).off(mw_event_name);
		$(window).on(mw_event_name,noscroll);
		if(e.originalEvent.wheelDelta<0 || e.originalEvent.detail>0){
			if(id<max-1){
				++id;
			}
			if(id==1){
				id=2;
			}
		}else{
			if(id>0){
				--id;
			}
			if(id==1){
				id=0;
			}
		}
		var element=$('div#block'+id).first();
		var el_top=element.offset().top;
		var el_h=element.height();
		var w_h=$(window).height();
		el_top-=(w_h-el_h)/2;
		$('body').animate({
			scrollTop: el_top
		},delay,function(){
			$(window).off(mw_event_name);
			$(window).on(mw_event_name,scroll);
		});
		e.preventDefault();
	}

	function noscroll(e){
		e.preventDefault();	
	}

	$(window).off(mw_event_name);
	$(window).on(mw_event_name,scroll);

	function scrollTo(e){
		$('.topmenu a , a.goto , .backButton a').off('click');
		$('.topmenu a , a.goto , .backButton a').on('click',noscroll);

		$(window).off(mw_event_name);
		$(window).on(mw_event_name,noscroll);

		var element=$($(this).attr('href'));
		var el_top=element.offset().top;
		var el_h=element.height();
		var w_h=$(window).height();
		el_top-=(w_h-el_h)/2;
		$('body').animate({
			scrollTop: el_top
		},delay,function(){
			$(window).off(mw_event_name);
			$(window).on(mw_event_name,scroll);
		});

		$('.topmenu a , a.goto , .backButton a').off('click');
		$('.topmenu a , a.goto , .backButton a').on('click',scrollTo);

		e.preventDefault();
	}

	$('.topmenu a , a.goto , .backButton a').on('click',scrollTo);

	$(document).scroll(function(){
		closest=$(document.elementFromPoint(1000,$(window).height()/2)).closest('.siteBlock_container');
		if(closest.length){
			id=closest.attr('id').replace('block','');
			backButton();
		}
	});

	backButton();
});