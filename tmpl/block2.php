					<img class="floatingImg floatingImg1" src="assets/img/img_penholder.png">
					<img class="floatingImg floatingImg2" src="assets/img/img_shtamp.png">
					<h1>Преимущества</h1>
					<div class="advantageBlock">
						<img src="assets/img/img_adv1.png">
						<p>Бесплатная доставка</p>
					</div>
					<div class="advantageBlock">
						<img src="assets/img/img_adv2.png">
						<p>Изготовление печати за 10 минут</p>
					</div>
					<div class="advantageBlock">
						<img src="assets/img/img_adv3.png">
						<p>Гарантия 10 лет</p>
					</div>
					<div class="advantageBlock">
						<img src="assets/img/img_adv4.png">
						<p>Оплата при получении товара</p>
					</div>
					<div class="advantageBlock">
						<img src="assets/img/img_adv5.png">
						<p>Документы можно предъявить при получении</p>
					</div>
					<div class="advantageBlock">
						<img src="assets/img/img_adv6.png">
						<p>Оплачивайте удобным для вас способом</p>
					</div>
					<div class="advantageBlock">
						<img src="assets/img/img_adv7.png">
						<p>Возможность забрать товар из любой точки города</p>
					</div>