<h1 class="white text-center">Наши партнеры</h1>
<div class="inline-block">
	<div class="column w-240">
		<img src="../assets/img/pch_yapona.png" class="block-center" height="131" width="131" alt="">
		<p class="simple bold white f15 m-t-20">
			Япона матрёна
		</p>
	</div>
	<div class="column w-240">
		<img src="../assets/img/pch_coca.png" class="block-center" height="131" width="131" alt="">
		<p class="simple bold white f15 m-t-20">
			Coca Cola
		</p>
	</div>
	<div class="column w-240">
		<img src="../assets/img/pch_ural.png" class="block-center" height="131" width="131" alt="">
		<p class="simple bold white f15 m-t-20">
			Урал ФД
		</p>
	</div>
	<div class="column w-240">
		<img src="../assets/img/pch_metall.png" class="block-center" height="131" width="131" alt="">
		<p class="simple bold white f15 m-t-20">
			МеталлИнвест Банк
		</p>
	</div>
</div>
<div class="inline-block m-t-40">
	<div class="column w-240">
		<img src="../assets/img/pch_vtb.png" class="block-center" height="131" width="131" alt="">
		<p class="simple bold white f15 m-t-20">
			ВТБ 24
		</p>
	</div>
	<div class="column w-240">
		<img src="../assets/img/pch_inomarka.png" class="block-center" height="131" width="131" alt="">
		<p class="simple bold white f15 m-t-20">
			Издательский дом <br>
			"Иномарка"
		</p>
	</div>
	<div class="column w-240">
		<img src="../assets/img/pch_pzsk.png" class="block-center" height="131" width="131" alt="">
		<p class="simple bold white f15 m-t-20">
			ПЗСК
		</p>
	</div>
</div>