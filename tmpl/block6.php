<h1>
	Советы при покупке
</h1>
<div class="gray-line">
	<span id="advice-slider-next"></span>
		<span class="big-num bold">1</span>
		<span class="small-num bold">2</span>
	<span id="advice-slider-prev"></span>
</div>
<ul class="advice-slider">
<? foreach($advices as $v){
	$text=$v['text'];
?>
	<li>
		<div class="inline-block">
			<div class="column w-240">
				<img src="../assets/img/folder.png" width="80%" alt="">
			</div>
			<div class="column w-720">
				<p class="simple text-justify">
					<?= $text ?>
				</p>
			</div>
		</div>
	</li>
<? } ?>
</ul>
<script>
	var number_of_slides = $("ul.advice-slider li").length;
	$(document).ready(function(){
	 	$('.advice-slider').bxSlider({
			nextSelector: '#advice-slider-next',
			prevSelector: '#advice-slider-prev',
	 		pager : false,
	 		onSlideNext : function () {
	 			old_num = Number($('.small-num').text());
	 			if (old_num == number_of_slides) {
	 				old_num = 1;
		 			$('.small-num').text(1);
		 			$('.big-num').text(number_of_slides);
	 			}else{
		 			$('.small-num').text(old_num+1);
		 			$('.big-num').text(old_num);
	 			};
	 		},
	 		onSlidePrev : function () {
	 			old_num = Number($('.big-num').text());
	 			if (old_num == 1) {
		 			$('.small-num').text(1);
		 			$('.big-num').text(number_of_slides);
	 			}else{
		 			$('.small-num').text(old_num);
		 			$('.big-num').text(old_num-1);
	 			};
	 		}
	 	});
	});
</script>