<img class="floatingImg floatingImg3" src="assets/img/img_jmi.png">
<div class="iPad">
	<div class="iPad_content">
		<div class="iPad_slot">
			<div class="iPad_slot_container">
				<div>
					<div class="valignHelper"></div>
					<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAkwBADs=">
				</div>
				<div>
					<div class="valignHelper"></div>
					<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAkwBADs=">
				</div>
			</div>
			<div class="iPad_slot_glass"></div>
		</div>
		<div class="iPad_slot">
			<div class="iPad_slot_container">
				<div>
					<div class="valignHelper"></div>
					<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAkwBADs=">
				</div>
				<div>
					<div class="valignHelper"></div>
					<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAkwBADs=">
				</div>
			</div>
			<div class="iPad_slot_glass"></div>
		</div>
		<div class="iPad_slot">
			<div class="iPad_slot_container">
				<div>
					<div class="valignHelper"></div>
					<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAkwBADs=">
				</div>
				<div>
					<div class="valignHelper"></div>
					<img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAkwBADs=">
				</div>
			</div>
			<div class="iPad_slot_glass"></div>
		</div>
		<div class="iPad_buttons">
			<a href="#" class="iPad_play">
				<div></div>
			</a>
			<a href="#" class="iPad_stop">
				<div></div>
			</a>
		</div>
		<div class="iPad_text">
			<p>3 штампа — <b>10%</b> скидка</p>
			<p>3 экрана — <b>20%</b> скидка</p>
			<p>3 конверта — <b>30%</b> скидка</p>
			<p>3 корзины — <b>40%</b> скидка</p>
		</div>
		<div class="iPad_text">
			<p>3 блокнота — <b>50%</b> скидка</p>
			<p>3 ручки — <b>60%</b> скидка</p>
			<p>3 телефона — <b>70%</b> скидка</p>
			<p>3 папки — <b>100%</b> скидка</p>
		</div>
	</div>
</div>
<div class="promo">
	<h2>!Викторина</h2>
	<p class="simple">
		Попытай свою удачу и выиграй огромную скидку на одну из печатей или штамп!
	</p>
	<div class="promo_result">
		<p class="simple">Ваш выигрыш:</p>
		<p class="promo_result_big"><b>0%</b>скидка</p>
		<p class="simple">Попробуйте снова</p>
		<div class="promo_result_bt"></div>
		<div class="promo_result_bl"></div>
	</div>
	<input type="button" id="orderButtonPromo" class="itemOrderButton orderShow" value="Заказать продукт"></input>
	<label for="orderButtonPromo"></label>
</div>