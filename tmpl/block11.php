<div class="contacts contacts_left">
	<p>
		Наш главный офис в Перми:
	</p>
	<p>
		ул. Героев Хасана 46, офис 424
	</p>
</div>
<div class="contacts contacts_left">
	<p>
		<a href="#" class="orderShow">Оставить заявку</a>
		/
		<a href="#" class="callbackShow">Обратный звонок</a>
	</p>
	<p>
		8 (342) 278-39-46, 276-54-58
	</p>
</div>
<div class="footerButtonContainer">
	<input type="button" id="orderButtonFooter" class="itemOrderButton orderShow" value="Заказать продукт">
	<label for="orderButtonFooter"></label>
</div>
<div class="footerBottom">
	<div class="footerLeft">
		<p>Официальный сайт — <a href="http://pechati-perm.ru/">pechati-perm.ru</a></p>
	</div>
	<div class="footerRight">
		<p>Разработка и дизайн сайта — <a href="http://milan-design.ru/">Milan design</a></p>
	</div>
	<img src="/assets/img/favicon.png">
</div>