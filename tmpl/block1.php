						<div class="itemDescr">
						<h2>
							<?= $name ?>
						</h2>
						<p class="simple">
							<?= $descr ?>
						</p>
						<p class="itemPrice">
							<b><?= $price ?></b> рублей
						</p>
						<input type="button" id="orderButton" class="itemOrderButton orderShow" value="Заказать продукт" data-id="<?= $id ?>"></input>
						<label for="orderButton"></label>
					</div>
					<div class="itemPhoto">
						<div class="valignHelper"></div>
						<img class="itemPhoto" src="assets/photo/<?= $img ?>.png">
					</div>