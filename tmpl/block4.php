<h1 class="yellow">Возможно вас заинтересует</h1>
<? foreach($items as $v){
	$name=$v['shortname'];
	$descr=$v['shortdescr'];
	$img=$v['img'];
	$id=$v['id'];
?>
<div class="interesting">
	<div class="interesting_img">
		<img src="assets/photo/<?= $img ?>_min.png">
	</div>
	<div class="interesting_txt">
		<p class="simple-bold"><?= $name ?></p>
		<div class="switch">
			<p class="simple-min"><?= $descr ?></p>
			<input type="button" class="orderButtonInteresting orderShow" value="Заказать" data-id="<?= $id ?>">
		</div>
	</div>
</div>
<? } ?>