<div class="keyBoard"></div>
<h1>Отзывы</h1>
<ul class="comment-slider">
<? foreach($comments as $v){ 
	$user=$v['user'];
	$status=$v['status'];
	$text=$v['text'];
?>
  <li>
	<div class="comment-user-block">
		<div class="pull-right">
			<img src="assets/img/user1.png" class="img-round comment-image" alt="">
		</div>
		<div class="pull-right text-right m-r-18">
			<div class="comment-head"><?= $user ?></div>
			<div class="comment-subhead"><?= $status ?></div>
		</div>
		<div class="text-justify simple pull-left m-t-40 relative">
			<div class="commas"></div>
			<?= $text ?>
		</div>
	</div>
  </li>
<? } ?>
</ul>
<script>
	$(document).ready(function(){
	 	$('.comment-slider').bxSlider({
	 		pager : false
	 	});
	});
</script>