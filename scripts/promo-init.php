<?

require_once('../includes/config.php');

session_start();

$res=array();

if(!isset($_SESSION['try_count'])){
	$_SESSION['try_count']=$try_count_max;
}

if(isset($_SESSION['try_start']) && $_SESSION['try_start']<strtotime('yesterday')){
	$_SESSION['try_count']=$try_count_max;
	unset($_SESSION['try_wait']);
}

if($_SESSION['try_count']<=0){
	$res['error']=array(1,'max_try_count_reached');
}else{
	if(!isset($_SESSION['try_wait'])){
		--$_SESSION['try_count'];
		$_SESSION['try_wait']=1;
		$_SESSION['try_seed']=mt_rand(0,$try_rand_limits);
		$_SESSION['try_start']=time();
		$res['try_count']=$_SESSION['try_count'];
		$res['try_seed']=$_SESSION['try_seed'];
		$res['try_limits']=$try_limits;
		$res['try_limits_sum']=$try_limits_sum;
		$res['try_delay']=$try_delay;
		$res['try_max_spin']=$try_max_spin;
		$res['try_stop_interval_max']=$try_stop_interval_max;
		$res['try_stop_interval_min']=$try_stop_interval_min;
		$res['try_rand_limits']=$try_rand_limits;
	}else{
		$res['error']=array(2,'already_playing');
	}
}

header('Content-Type: application/json');
echo json_encode($res);

?>