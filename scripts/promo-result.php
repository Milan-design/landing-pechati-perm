<?

require_once('../includes/config.php');
require_once('../includes/database.php');

session_start();

$randSeed=295020;
$startTime=0;
$stopTime=0;
$spin_total=0;
$spin_after_stop=0;

$stopInterval=0;

function randNum(){
	global $randSeed, $try_rand_limits, $startTime, $stopTime, $spin_total, $spin_after_stop;
	return $randSeed=round(sin(1533.1*$randSeed+sqrt(1.1*$stopTime))*(418713678.1+1.1*$spin_after_stop)+sqrt(100500.1+1.1*$randSeed*$randSeed*12345.1+1.1*$spin_total)/2+cos(1.1*$randSeed*$randSeed)*(sqrt(1.1*$startTime)+12345.1))%$try_rand_limits;
}

function randId(){
	global $randSeed, $try_limits, $try_limits_sum;
	randNum();
	$limit=abs($randSeed)%$try_limits_sum+1;
	$i=0;
	while($limit>0){
		$limit-=$try_limits[$i];
		++$i;
	}
	return $i;
}

function randInterval(){
	global $randSeed, $try_stop_interval_min, $try_stop_interval_max;
	randNum();
	return abs($randSeed)%($try_stop_interval_max-$try_stop_interval_min)+$try_stop_interval_min;
}

if(isset($_SESSION['try_wait'])){
	if(isset($_SESSION['try_seed']) && isset($_SESSION['try_start'])){
		$randSeed=$_SESSION['try_seed'];
		$session_starttime=$_SESSION['try_start'];
		if(isset($_POST['json'])){
			$json=json_decode($_POST['json'],true);
			if($json!==null && isset($json['result']) && isset($json['start']) && isset($json['stop'])){
				$startTime=$json['start'];
				$stopTimeTotal=$json['stop'];
				$resultTotal=$json['result'];
				if($startTime<$stopTimeTotal && ($stopTimeTotal-$startTime)/$try_delay<=$try_max_spin /* && ($stopTimeTotal-$startTime)<=(microtime()-$_SESSION['try_start'])*/){
					$stop=false;
					$wheel_count=3;
					$result=array();
					$currentTime=$startTime;
					while($wheel_count){
						++$spin_total;
						$currentTime+=$try_delay;
						if($currentTime==$stopTimeTotal && !$stop){
							$stop=true;
							$stopTime=$stopTimeTotal;
							$stopInterval=randInterval();
						}

						if($stop){
							++$spin_after_stop;
							$stopInterval-=$try_delay;
							if($stopInterval<=0){
								--$wheel_count;
								if($wheel_count){
									$stopInterval=randInterval();
								}else{
									break;
								}
							}
						}

						if($wheel_count){
							for($i=0;$i<$wheel_count;++$i){
								$id=randId();
								if((!$i) && $stop && ($stopInterval<=$try_delay)){
									$result[]=$id;
								}
							}
						}
					}
					if($result==$resultTotal){
						$res['win']=($result[0]==$result[1] && $result[0]==$result[2]);
						if($res['win']){
							$v=$result[0];
							if($v<8){
								$res['percent']=$v.'0';
							}else{
								$res['percent']='100';
							}
							$item=$db->select(array('id','microname'),1,'item',new MySQLTerm('RAND()'),1)[0];
							$res['item']=array(
								'name'=>$item['microname'],
								'id'=>$item['id']
							);
							$res['id']=$db->insert(array('item'=>$item['id'],'percent'=>$res['percent']),'promo_results');
							$res['code']=hash('crc32b',$randSeed.date(DATE_RFC2822).$res['id'].'salt');
							$db->update(array('hash'=>$res['code']),array('id'=>$res['id']),'promo_results');
						}
					}else{
						$res['error']=array(16,'result_not_match');
					}
				}else{
					$res['error']=array(15,'cheating_detected');
				}
			}else{
				$res['error']=array(14,'missing_data');
			}
		}else{
			$res['error']=array(13,'no_data');
		}
	}else{
		$res['error']=array(12,'internal_data_error');
	}
	unset($_SESSION['try_wait']);
}else{
	$res['error']=array(11,'not_playing');
}

header('Content-Type: application/json');
echo json_encode($res);

?>