<?

require_once('../includes/config.php');

session_start();

$res=array();

if(isset($_SESSION['try_count']) && $_SESSION['try_count']<=0){
	$res['error']=array(1,'max_try_count_reached');
}else{
	if(isset($_SESSION['try_wait'])){
		$res['error']=array(2,'already_playing');
	}else{
		$res['ready']=1;
	}
}

header('Content-Type: application/json');
echo json_encode($res);

?>